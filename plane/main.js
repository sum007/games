
var enableDebugMode = function(game, enable) {
    if(!enable) {
        return
    }
    window.paused = false
    window.addEventListener('keydown', function(event){
        var k = event.key
        if (k == 'p') {
            // 暂停功能
            window.paused = !window.paused
        } else if ('1234567'.includes(k)) {
            // 为了 debug 临时加的载入关卡功能
            blocks = loadLevel(game, Number(k))
        }
    })
    // 控制速度
    document.querySelector('#id-input-speed').addEventListener('input', function(event) {
        var input = event.target
        // log(event, input.value)
        window.fps = Number(input.value)
    })
}
const config = {
    cloud_speed:1,
    fps:30,
    cooldown: 10,
    player_speed: 10,
    bullet_speed: 10,
    enemy_speed: 10,
}
const debugModel = function() {
    bindAll('.input-slider', 'input', function(event) {
        var target = event.target
        var configKey = target.dataset.value
        var value = target.value
        //eval(configKey + '=' + value)
        config[configKey] = Number(value)
        var lable = target.closest('label').querySelector('.span-value')
        lable.innerText = value
    })
}
var __main = function() {
    var images = {
        bullet1:'img/bullet/b1.png',
        bullet2:'img/bullet/b2.png',
        bullet3:'img/bullet/b3.png',
        bullet4:'img/bullet/b4.png',
        bullet5:'img/bullet/b5.png',
        bg1: 'img/background/bg3.jpg',
        player1: 'img/player/p4.png',
        bullet1: 'img/bullet/b1.png',
        cloud1: 'img/cloud/cloud1.png',
        cloud2: 'img/cloud/cloud2.png',

        enemy1: 'img/enemy/e1.png',
        enemy2: 'img/enemy/e2.png',
        enemy3: 'img/enemy/e3.png',
        enemy4: 'img/enemy/e4.png',
        enemy5: 'img/enemy/e5.png',
        enemy6: 'img/enemy/e6.png',
        enemy7: 'img/enemy/e7.png',
        enemy8: 'img/enemy/e8.png',
        enemy9: 'img/enemy/e9.png',
        enemy10: 'img/enemy/e10.png',
        enemy11: 'img/enemy/b1.png',
        enemy12: 'img/enemy/b2.png',
        fire: 'img/boom/boom1.png',
    }
    var game = GuaGame.instance(30, images, function(g){
        var s = Scene.new(g)
        g.runWithScene(s)
    })
}

__main()
