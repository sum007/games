class SceneTitle extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
        game.registerAction('k', function(){
            var s = Scene(game)
            game.replaceScene(s)
        })
    }
    setup(){
        var text = '按 k 开始游戏'
        var label = Label.new(this.game, text, 150, 190)
        var ps = ParticleSystem.new(this.game, 100, 200)
        this.addElement(ps)
        this.addElement(label)
    }
    draw() {
        super.draw()
        // draw labels
        // this.game.context.fillText('按 k 开始游戏', 150, 190)
    }
}
