class Bullet extends CreatImg {
    constructor(game, name, type) {
        super(game, name)
        this.type = type
        this.setup()
    }
    kill(){
        this.alive = false
    }
    setup(){
        this.alive = true
        this.speed = 10
    }
    update(){
        if(this.type){
            this.y -= this.speed
        }else {
            this.y += this.speed
        }
    }
    draw(){
        if (this.alive) {
            this.drawImage(this)
        }
    }
    collide(b) {
        return this.alive && (rectIntersects(this, b) || rectIntersects(b, this))
    }
}
