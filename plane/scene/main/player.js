class Player extends CreatImg {
    constructor(game) {
        super(game, 'player1')
        this.speed = 10
        this.x = 200
        this.y = 450
        this.alive = true
        this.lifes = 4
        this.cooldown = 0
        this.alpha = 1
        this.life_down = false
    }
    update(){
        if (this.cooldown > 0) {
            this.cooldown--
        }
        if (this.ps && this.ps.lifes < 1) {
            this.scene.elements.splice(this.index, 1)
            this.ps = null
        }
        if (this.life_down && this.alpha > 0 ) {
            this.alpha = 0.2
            this.life_down = false
        }
    }
    kill(){
        this.life_down = true
        this.lifes--
        if (this.lifes < 1) {
            this.alive = false
            var x = this.x
            var y = this.y
            this.ps = ParticleSystem.new(this.game, x, y)
            this.index = this.scene.elements.length
            this.scene.addElement(this.ps)
        }
    }
    fire(){
        if (!this.alive) {
            return
        }
        if (this.cooldown == 0) {
            this.cooldown = 3
            var x = this.x + this.width/2
            var y = this.y
            var b = Bullet.new(this.game, 'bullet2', 1)
            b.x = x - b.width/2
            b.y = y
            this.scene.addElement(b)
            this.scene.bullets_player.push(b)
        }
    }
    move(axes = 'x', value = 200, start = 0, end = 512){
        this[axes] = value
        if(value < start){
            this[axes] = start
        }
        var l = axes == 'x'? this.width : this.height
        if(this[axes]> end - l){
            this[axes]= end - l
        }
    }
    moveLeft(){
        this.x -= this.speed
        this.move('x', this.x, 0, 512)
    }
    moveRight(){
        this.x += this.speed
        this.move('x', this.x, 0, 512)
    }
    moveUp(){
        this.y -= this.speed
        this.move('y', this.y, 0, 650)
    }
    moveDown(){
        this.y += this.speed
        this.move('y', this.y, 0, 650)
    }
    debug(){
        this.speed = config.player_speed
    }
    draw(){
        //this.alive && this.drawImage(this)
        if (!this.alive) {
            return
        }
        var context = this.game.context
        context.save()
        context.globalAlpha = this.alpha
        context.drawImage(this.texture,this.x, this.y)
        context.restore()
        this.alpha = 1
    }
}
