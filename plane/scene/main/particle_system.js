class ParticleSystem {
    constructor(game, x, y) {
        this.x = x
        this.y = y
        this.game = game
        this.setup()
    }
    static new(game, x, y) {
        var i = new this(game, x, y)
        return i
    }
    setup(){
        this.lifes = 10
        this.partNum = 1000
        this.particles = []
    }
    draw() {
        for (var p of this.particles) {
            p.draw()
        }
    }
    update(){
        this.lifes--
        if (this.particles.length < this.partNum) {
            var p = Particle.new(this.game)
            var s = 2
            var vx = randomRange(-s, s)
            var vy = randomRange(-s, s)
            p.init(this.x, this.y, vx, vy)
            this.particles.push(p)
        }
        for (var p of this.particles) {
            p.update()
        }
        this.particles = this.particles.filter(p => p.life > 0)
    }
}
