class Scene extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
        this.setupInputs()
    }
    setup(){
        this.bullets_player = []
        this.bullets_enemy = []
        var t = this
        var g = t.game
        t.enemyNum = 3
        t.cloudNum = 2
        t.score = 0
        t.bg = CreatImg.new(g, 'bg1')
        t.player = Player.new(g)
        t.addElement(t.bg)
        t.addClouds()
        t.addElement(t.player)
        t.addEneies()


    }
    addEneies(){
        var es = []
        for (var i = 0; i < this.enemyNum; i++) {
            var e = Enemy.new(this.game)
            es.push(e)
            this.addElement(e)
        }
        this.enemies = es
    }
    addClouds(){
        var es = []
        for (var i = 0; i < this.cloudNum; i++) {
            var e = Cloud.new(this.game)
            es.push(e)
            this.addElement(e)
        }
        this.clouds = es
    }
    setupInputs(){
        var g = this.game
        var t = this
        g.registerAction('a', function() {
            t.player.moveLeft()
        })
        g.registerAction('d', function functionName() {
            t.player.moveRight()
        })
        g.registerAction('w', function functionName() {
            t.player.moveUp()
        })
        g.registerAction('s', function functionName() {
            t.player.moveDown()
        })
        g.registerAction('o', function functionName() {
            t.player.fire()
        })
    }
    draw(){
        var t = this
        super.draw()
        this.game.context.fillStyle = '#fff'
        this.game.context.font = "20px Arial"
        this.game.context.fillText('分数: ' + this.score, 10, 630)
    }
    collider(c1, c2, callback){
        var es = c1
        var bs = c2
        if(es.length < 1) return
        for (var i = 0; i < es.length; i++) {
            var e = es[i]
            for (var j = 0; j < bs.length; j++) {
                var b = bs[j]
                if (e.collide(b) && b.alive) {
                    e.kill()
                    b.kill()
                    callback && callback(e)
                }
            }
        }
    }
    update(){
        super.update()
        var me = this
        var es = this.enemies
        var bs = this.bullets_player
        var ebs = this.bullets_enemy
        var player = this.player
        //玩家子弹与敌机相撞
        me.collider(es, bs, function(e) {
            if (e.alive == false) {
                me.score += 100
            }
        })
        //敌机子弹与玩家相碰
        if(ebs.length){
            for (var i = 0; i < ebs.length; i++) {
                var e = ebs[i]
                if (e.collide(player) && player.alive) {
                    e.kill()
                    player.kill()
                }
            }
        }
        //敌机子弹与玩家子弹相撞
        me.collider(ebs, bs)
        //敌机和玩家相碰
        if(es.length){
            for (var i = 0; i < es.length; i++) {
                var e = es[i]
                if (e.collide(player) && player.alive) {
                    e.kill()
                    player.kill()
                }
            }
        }
    }
}
