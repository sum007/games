class Enemy extends CreatImg {
    constructor(game) {
        var type = randomRange(0, 10)
        var name = 'enemy' + type
        super(game, name)
        this.setup()
    }
    setup(){
        this.alive = true
        this.cooldown = 0
        this.lifes = randomRange(1 , 2)
        this.speed = randomRange(2, 4)
        this.x = randomRange(0, 330)
        this.y = -randomRange(0, 300)
    }
    update(){
        super.update()
        this.fire()
        this.y += this.speed
        if (this.y > 650) {
            this.setup()
        }
        if (this.ps && this.ps.lifes < 1) {
            this.scene.elements.splice(this.index, 1)
            this.ps = null
        }
        if (this.cooldown > 0) {
            this.cooldown--
        }
    }
    fire(){
        if (!this.alive) {
            return
        }
        if (this.cooldown == 0) {
            this.cooldown = 30
            var x = this.x + this.width/2
            var y = this.y
            var b = Bullet.new(this.game, 'bullet1', 0)
            b.x = x - b.width/2
            b.y = y + this.height
            b.speed = 7
            this.scene.addElement(b)
            this.scene.bullets_enemy.push(b)
        }
    }
    kill(){
        this.lifes--
        if (this.lifes < 1) {
            this.alive = false
            var x = this.x
            var y = this.y
            this.ps = ParticleSystem.new(this.game, x, y)
            this.index = this.scene.elements.length
            this.scene.addElement(this.ps)
        }
    }
    collide(b) {
        return this.alive && (rectIntersects(this, b) || rectIntersects(b, this))
    }
    draw(){
        if (this.alive) {
            this.drawImage(this)
        }
    }
}
