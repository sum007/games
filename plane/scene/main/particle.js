class Particle extends CreatImg {
    constructor(game) {
        var name = 'fire'
        super(game, name)
        this.setup()
    }
    setup(){
        this.life = 30
    }
    init(x, y, vx, vy){
        this.x = x
        this.y = y
        this.vx = vx
        this.vy = vy
    }
    update(){
        this.life--
        this.x += this.vx
        this.y += this.vy
        var factor = 0.01
        this.vx -= factor*this.vx
        this.vy -= factor*this.vy
    }
    draw(){
        super.draw()
    }
}
