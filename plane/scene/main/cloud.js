class Cloud extends CreatImg {
    constructor(game) {
        var type = randomRange(0, 2)
        var name = 'cloud' + type
        super(game, name)
        this.setup()
    }
    setup(){
        this.speed = 0.5
        this.x = randomRange(0, 200)
        this.y = -randomRange(0, 300)
    }
    update(){
        super.update()
        this.y += this.speed
        if (this.y > 650) {
            this.setup()
        }
    }
    draw(){
        this.drawImage(this)
    }
}
