class Player extends CreatImg {
    constructor(game) {
        super(game, 'player1')
        this.speed = 10
        this.x = 200
        this.y = 450
        this.cooldown = 0
    }
    update(){
        if (this.cooldown > 0) {
            this.cooldown--
        }
    }
    fire(){
        if (this.cooldown == 0) {
            this.cooldown = 10
            var x = this.x + this.width/2
            var y = this.y
            var b = Bullet.new(this.game)
            b.x = x - b.width/2
            b.y = y
            this.scene.addElement(b)
            this.scene.bullets.push(b)
        }
    }
    move(axes = 'x', value = 200, start = 0, end = 512){
        this[axes] = value
        if(value < start){
            this[axes] = start
        }
        var l = axes == 'x'? this.width : this.height
        if(this[axes]> end - l){
            this[axes]= end - l
        }
    }
    moveLeft(){
        this.x -= this.speed
        this.move('x', this.x, 0, 512)
    }
    moveRight(){
        this.x += this.speed
        this.move('x', this.x, 0, 512)
    }
    moveUp(){
        this.y -= this.speed
        this.move('y', this.y, 0, 650)
    }
    moveDown(){
        this.y += this.speed
        this.move('y', this.y, 0, 650)
    }
    debug(){
        this.speed = config.player_speed
    }
    draw(){
        this.drawImage(this)
    }
}
