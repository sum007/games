const e = sel => document.querySelector(sel)

const log = console.log.bind(console)

const es = (sel) => document.querySelectorAll(sel)
const bindAll = function(selector, eventName, callback) {
    var elements = es(selector)
    for (var i = 0; i < elements.length; i++) {
        var ele = elements[i]
        ele.addEventListener(eventName, function(event) {
            callback && callback(event)
        })
    }
}
const imageFromPath = function(path) {
    var img = new Image()
    img.src = path
    return img
}

const rectIntersects = function(a, b) {
    var p = (a.x + a.texture.width) < b.x || a.x > (b.x + b.texture.width) || (a.y+ a.texture.height) < b.y || a.y > (b.y + b.texture.height)
    return !p
}
const randomRange = function(start, end){
    var n = Math.random()*(end - start)
    var m = Math.ceil(n + start)
    return m
}
