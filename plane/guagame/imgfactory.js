class CreatImg {
    constructor(game, name) {
        this.game = game
        this.texture = game.textureByName(name)
        this.x = 0
        this.y = 0
        this.width = this.texture.width
        this.height = this.texture.height
    }
    static new(...args) {
        var i = new this(...args)
        return i
    }
    update(){

    }
    drawImage(img) {
        this.game.context.drawImage(img.texture, img.x, img.y)
    }
    draw(){
        this.drawImage(this)
    }
}
