class SceneTitle extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }

    setup(){
        this.score = 0
        var bg = CreatImg.new(this.game, 'bg_day')
        //
        var s1 = CreatImg.new(this.game, 'ready')
        s1.x = 90
        s1.y = 160
        //
        var tutorial = CreatImg.new(this.game, 'tutorial')
        tutorial.x = 120
        tutorial.y = 250
        //add pipes
        this.addElement(bg)
        this.addElement(s1)
        this.addElement(tutorial)
        this.lands = []
        this.vLand = -1
        for (var i = 0; i < 2; i++) {
            var land = CreatImg.new(this.game, 'land')
            land.x = i * 288
            land.y = 560
            this.addElement(land)
            this.lands.push(land)
        }
        this.bird = Animation.new(this.game)
        this.bird.x = 60
        this.bird.y = 250
        this.bird.vy = 0
        this.bird.gy = 0
        this.addElement(this.bird)
        this.setupInputs()
        var ctx = this.game.context
        var text = `Press K To Start`
        ctx.font = "30px Arial"
        ctx.fillStyle = '#fff'
        var t = Label.new(this.game, text, 65, 400)
        this.addElement(t)
        var l = Label.new(this.game, this.score, 170, 100)
        this.addElement(l)
    }
    setupInputs(){
        this.game.registerAction('k', (keyStatus) => {
            var s = Scene.new(this.game)
            this.game.replaceScene(s)
        })
    }
    draw() {
        super.draw()
    }
}
