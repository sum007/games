class Animation {
    constructor(game) {
        this.game = game
        this.setup()
    }
    static new(game) {
        var i = new this(game)
        return i
    }
    setup(){
        this.animations = {
            idle: [],
            run: [],
        }
        for (var i = 1; i < 4; i++) {
            var name = `b${i}`
            var t = this.game.textureByName(name)
            this.animations['idle'].push(t)
        }
        this.animationName = 'idle'
        this.texture = this.frames()[0]
        this.width = this.texture.width
        this.height = this.texture.height
        this.frameIndex = 0
        this.frameCount = 3
        this.flipX = false
        this.gy = 10
        this.vy = 0
        this.rotation = 0
        this.alpha = 1
        this.start = false
        this.alive = true
    }
    draw() {
        var context = this.game.context
        context.save()
        var w2 = this.width / 2
        var h2 = this.height / 2
        context.translate(this.x + w2, this.y + h2)
        if (this.flipX) {
            context.scale(-1, 1)
        }
        context.globalAlpha = this.alpha
        context.rotate(this.rotation * Math.PI / 180)
        context.translate(-w2, -h2)
        context.drawImage(this.texture, 0, 0)
        context.restore()
    }
    update(){
        if (this.alpha > 0 && !this.alive) {
            this.alpha -= 0.05
        }
        this.y += this.vy
        this.vy += this.gy * 0.2
            if (this.y >= 532) {
            this.y = 532
        }
        this.frameCount--
        if (this.frameCount == 0) {
            this.frameCount = 3
            this.frameIndex = (this.frameIndex + 1) % this.frames().length
            this.texture = this. frames()[this.frameIndex]
        }
        //更新角度
        if (this.rotation < 45 && this.vy != 0) {
            this.rotation += 5
        }
    }
    move(x, keyStatus){
        this.flipX = x < 0
        this.x += x
    }
    frames(){
        return this.animations[this.animationName]
    }
    changeAnimation(name){
        this.animationName = name
    }
    jump(){
        if (!this.alive) {
            return
        }
        this.vy = -10
        this.rotation = -45
    }
    debug(){

    }
    dead(){
        this.alive = false
    }
}
