
class Scene extends GuaScene {
    constructor(game) {
        super(game)
        this.setup()
    }
    setup(){
        this.score = 0
        var bg = CreatImg.new(this.game, 'bg_day')
        this.dead_title = CreatImg.new(this.game, 'dead')
        this.dead_title.x = 90
        this.dead_title.y = 250
        //add pipes
        this.pipe = Pipes.new(this.game)
        this.addElement(bg)
        this.addElement(this.pipe)
        this.lands = []
        this.vLand = -1
        for (var i = 0; i < 2; i++) {
            var land = CreatImg.new(this.game, 'land')
            land.x = i * 288
            land.y = 560
            this.addElement(land)
            this.lands.push(land)
        }
        this.bird = Animation.new(this.game)
        this.bird.x = 60
        this.bird.y = 250
        this.addElement(this.bird)
        this.setupInputs()
        var ctx = this.game.context
        ctx.font = "50px Arial"
        ctx.fillStyle = '#fff'
        var l = Label.new(this.game, this.score, 150, 100)
        this.addElement(l)
    }
    setupInputs(){
        // this.game.registerAction('a', (keyStatus) => {
        //     this.bird.move(-2, keyStatus)
        // })
        // this.game.registerAction('d', (keyStatus) => {
        //     this.bird.move(2, keyStatus)
        // })
        this.game.registerAction('j', (keyStatus) => {
            this.bird.jump()
        })
        this.game.registerAction('r', (keyStatus) => {
            var s = SceneTitle.new(this.game)
            this.game.replaceScene(s)
        })
    }
    start(){

    }
    addTitle(text){

    }
    update(){
        var b = this.bird
        super.update()
        var ebs = this.pipe.pipes

        for (var i = 0; i < 2; i++) {
            this.lands[i].x += this.vLand
            if (this.lands[i].x < -287) {
                this.lands[i].x = 288
            }

        }
        if(ebs.length){
            for (var i = 0; i < ebs.length; i += 2) {
                var e = ebs[i]
                if (e.x == 170) {
                    this.score++
                    var ctx = this.game.context
                    ctx.font = "80px Cursive";
                    var l = Label.new(this.game, this.score, 150, 100)
                    this.elements.splice(-1, 1)
                    this.addElement(l)
                }
                if (b.y > 531 || rectIntersects(e, b)) {
                    this.vLand = 0
                    this.pipe.dead()
                    b.dead()
                    this.addElement(this.dead_title)
                    var ctx = this.game.context
                    var text = `Press R To Restart`
                    ctx.font = "30px Arial"
                    ctx.fillStyle = '#fff'
                    var l = Label.new(this.game, text, 60, 350)
                    this.addElement(l)
                }
            }
        }

    }
}
