class Pipes {
    constructor(game) {
        this.game = game
        this.setup()
    }
    setup(){
        this.vPipe = -5
        this.alive = true
        this.pipes = []
        this.pipeSpaceY = 150
        this.pipeSpaceX = 200
        this.columsOfpipe = 3
        for (var i = 0; i < this.columsOfpipe ; i++) {
            var p1 = CreatImg.new(this.game, 'pipe')
            p1.x = 500 + i * this.pipeSpaceX
            var p2 = CreatImg.new(this.game, 'pipe')
            p2.x = p1.x
            p2.flipY = true
            this.resetPipesPosition(p1, p2)
            this.pipes.push(p1)
            this.pipes.push(p2)
        }
    }
    static new(game){
        return new this(game)
    }
    resetPipesPosition(p1, p2){
        p1.y = randomRange(-200, 30)
        p2.y = p1.y + p1.height + this.pipeSpaceY
    }
    update(){
        if (!this.alive) {
            return
        }
        for (var i = 0; i < this.pipes.length; i += 2) {
            var p1 = this.pipes[i]
            var p2 = this.pipes[i + 1]
            p1.x += this.vPipe
            p2.x += this.vPipe
            if (p1.x < -100) {
                p1.x += this.pipeSpaceX * this.columsOfpipe
            }
            if (p2.x < -100) {
                p2.x += this.pipeSpaceX * this.columsOfpipe
                this.resetPipesPosition(p1, p2)
            }
        }
    }
    draw(){
        var context = this.game.context
        for (var p of this.pipes) {
            context.save()
            var w2 = p.width / 2
            var h2 = p.height / 2
            context.translate(p.x + w2, p.y + h2)
            var scaleX = p.flipX? -1 : 1
            var scaleY = p.flipY? -1 : 1
            context.scale(scaleX, scaleY)
            context.rotate(p.rotation * Math.PI / 180)
            context.translate(-w2, -h2)
            context.drawImage(p.texture, 0, 0)
            context.restore()
        }
    }
    debug(){
        this.pipeSpaceX = config.pipeSpaceX.value
        this.pipeSpaceY = config.pipeSpaceY.value
    }
    dead(){
        this.alive = false
    }
}
