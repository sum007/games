
var __main = function() {
    var images = {
        //flappy bird
        bg_day: 'img/bird/bg_day.png',
        land: 'img/bird/land.png',
        b1: 'img/bird/bird1.png',
        b2: 'img/bird/bird2.png',
        b3: 'img/bird/bird3.png',
        pipe: 'img/bird/pipe.png',
        ready: 'img/bird/text_ready.png',
        tutorial :'img/bird/tutorial.png',
        dead: 'img/bird/text_game_over.png',
    }
    var game = GuaGame.instance(30, images, function(g){
        var s = SceneTitle.new(g)
        g.runWithScene(s)
    })
}

__main()
