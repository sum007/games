const bindEvents = function () {
    bindAll('.input-slider', 'input', function(event) {
        var target = event.target
        var configKey = target.dataset.value
        var value = target.value
        eval(configKey + '.value =' + value)
        //config[configKey] = Number(value)
        var lable = target.closest('label').querySelector('.span-value')
        lable.innerText = value
    })
}
const templateControl = function (key, item) {
    var t = `
    <div class="">
        <label>
            <input class="input-slider" type = 'range'
            max=300 value ='${item.value}' data-value='config.${key}'>
            ${item._comment}：<span class="span-value"></span>
        </label>
    </div>
    `
    return t
}
const insertControls = function () {
    var div = e('.bird-controls')
    var keys = Object.keys(config)
    for (var k of keys) {
        var item = config[k]
        var html = templateControl(k, item)
        div.insertAdjacentHTML('beforeend', html)
    }
}
var debug_start = function(){
    insertControls()
    bindEvents()
}
debug_start()
