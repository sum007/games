class GuaScene {
    constructor(game) {
        this.game = game
        this.debugMode = 1
        this.elements = []
    }
    static new(game) {
        var i = new this(game)
        return i
    }
    addElement(ele){
        ele.scene = this
        this.elements.push(ele)
    }
    draw() {
        var es = this.elements
        for (var i = 0; i < es.length; i++) {
            var e = es[i]
            e.draw()
        }
    }
    update() {
        var es = this.elements
        if (this.debugMode == 1) {
            for (var i = 0; i < es.length; i++) {
                var e = es[i]
                e.debug&&e.debug()
            }
        }
        for (var i = 0; i < es.length; i++) {
            var e = es[i]
            e.update()
        }
    }
}
