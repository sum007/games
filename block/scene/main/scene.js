var Scene = function(game) {
    var s = {
        game: game,
    }
    // 初始化
    var paddle = Paddle(game)
    var ball = Ball(game)

    var score = 0

    //var blocks = loadLevel(game, 5)
    var blocks = loadLe(game)

    game.registerAction('a', function(){
        if (!ball.fired) {
            ball.moveLeft()
        }
        paddle.moveLeft()
    })
    game.registerAction('d', function(){
        if (!ball.fired) {
            ball.moveRight()
        }
        paddle.moveRight()
    })
    game.registerAction('f', function(){
        ball.fire()
    })

    s.draw = function() {
        // draw 背景
        game.context.fillStyle = "#88dbd5"
        game.context.fillRect(0, 0, 400, 300)
        // draw
        game.drawImage(paddle)
        game.drawImage(ball)
        // draw blocks
        for (var i = 0; i < blocks.length; i++) {
            var block = blocks[i]
            if (block.alive) {
                game.drawImage(block)
            }
        }
        // draw labels
        game.context.fillStyle = 'red'
        game.context.fillText('分数: ' + score, 10, 290)
    }
    s.update = function() {
        if (window.paused) {
            return
        }

        ball.move()
        // 判断游戏结束
        if (ball.y > paddle.y) {
            // 跳转到 游戏结束 的场景
            var end = SceneEnd.new(game)
            game.replaceScene(end)
        }
        // 判断相撞
        if (paddle.collide(ball)) {
            // 这里应该调用一个 ball.反弹() 来实现
            ball.反弹()
        }
        // 判断 ball 和 blocks 相撞
        for (var i = 0; i < blocks.length; i++) {
            var block = blocks[i]
            if (rectIntersects(block, ball) && block.alive ) {
                //log('block 相撞')
                block.kill()
                ball.反弹()
                // 更新分数
                if (block.x) {
                    score += 100
                }

            }
        }
    }
    // mouse event
    var enableDrag = false
    var enableEdit = false
    game.canvas.addEventListener('mousedown', function(event) {
        var x = event.offsetX
        var y = event.offsetY
        // 检查是否点中了 ball
        if (ball.hasPoint(x, y)) {
            // 设置拖拽状态
            enableDrag = true
        }
        enableEdit = true
        var arr = []
        arr[0] = levelFormat(x, y).x
        arr[1] = levelFormat(x, y).y
        var key = `${arr[0]}_${arr[1]}`
        if (u[key] && u[key].length > 0) {
            u[key] = []
        }else {
            u[key] = arr
        }
        blocks = loadLe(game)
    })
    game.canvas.addEventListener('mousemove', function(event) {
        var x = event.offsetX
        var y = event.offsetY
        if (enableDrag) {
            ball.x = x
            ball.y = y
        }
        if (enableEdit) {
            var arr = []
            arr[0] = levelFormat(x, y).x
            arr[1] = levelFormat(x, y).y
            var key = `${arr[0]}_${arr[1]}`
            u[key] = arr
            blocks = loadLe(game)
        }
    })
    game.canvas.addEventListener('mouseup', function(event) {
        enableDrag = false
        enableEdit = false
    })

    return s
}
