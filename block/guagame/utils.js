var e = sel => document.querySelector(sel)

var log = console.log.bind(console)

var imageFromPath = function(path) {
    var img = new Image()
    img.src = path
    return img
}

const rectIntersects = function(a, b) {
    var p = (a.x + a.image.width) < b.x || a.x > (b.x + b.image.width) || (a.y+ a.image.height) < b.y || a.y > (b.y + b.image.height)
    return !p
}
var levelFormat = function(x, y) {
    var x0 = -40
    var y0 = -19
    var dx = 40
    var dy = 19
    var s = {}
    for (let i = 0; i < 100; i++) {
        x0 += dx
        x1 = x0 + dx
        if ( x0 <= x && x < x1) {
            s.x = x0
            break
        }
    }
    for (let j = 0; j < 100; j++) {
        y0 += dy
        y1 = y0 + dy
        if ( y0 <= y && y < y1) {
            s.y = y0
            break
        }
    }
    return s
}
